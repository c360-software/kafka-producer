package com.tullynore.kafka.producer.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class ProduceHandler {

    private KafkaTemplate<Integer, String> kafkaTemplate;
    private String topicName;

    public ProduceHandler(KafkaTemplate<Integer, String> kafkaTemplate,
                          @Value("${kafka.topic}") String topicName) {
        this.kafkaTemplate = kafkaTemplate;
        this.topicName = topicName;
    }

    @GetMapping("/produce")
    public String hello(@RequestParam String message, @RequestParam Integer id) throws Exception {
        log.info("Sending the message " + message);
        SendResult<Integer, String> result = kafkaTemplate.send(topicName, message).get();
        log.info("Result partition" + result.getRecordMetadata().partition());
        return "Sent to Kafka";
    }
}
